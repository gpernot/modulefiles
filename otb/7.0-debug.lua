-- -*- lua -*-

help(
[[
OTB with debug symbols
]])

name = "otb-debug"
version = "7.0-debug"

load("otb/7.0-generic")

pushenv("CMAKE_BUILD_TYPE", "Debug")
pushenv("GDAL_DATA", "/usr/share/gdal")
