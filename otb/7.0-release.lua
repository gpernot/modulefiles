-- -*- lua -*-

help(
[[
OTB Release version
]])

name = "otb-release"
version = "7.0-release"

load("otb/7.0-generic")

pushenv("CMAKE_BUILD_TYPE", "Release")
pushenv("CC", "gcc")
pushenv("CXX", "g++")
pushenv("GDAL_DATA", "/usr/share/gdal")
