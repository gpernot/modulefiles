-- -*- lua -*-

help(
[[
OTB with debug symbols
]])

whatis("Module name : "..name)
whatis("Version : "..version)

local home = pathJoin(os.getenv("HOME"), "usr", name, version)

prepend_path("PATH", pathJoin(home, "bin"))
prepend_path("CPATH", pathJoin(home, "include"))
prepend_path("LD_LIBRARY_PATH", pathJoin(home, "lib"))

prepend_path("PYTHONPATH", pathJoin(home, "lib/python3.6/site-packages"))

pushenv("CMAKE_INSTALL_PREFIX", home)
pushenv("CMAKE_BUILD_TYPE", "Release")
pushenv("OTB_APPLICATION_PATH", home)
pushenv("CC", "gcc-8")
pushenv("CXX", "g++-8")
-- pushenv("CMAKE_C_COMPILER", "gcc-7")
-- pushenv("CMAKE_CXX_COMPILER", "g++-7")

build_otb_command = [[
mkdir -p build;
pushd build;
cmake
  -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE:-Release}
  -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX:-$HOME/usr}
  -DBUILD_TESTING=ON
  -DOTB_USE_GLEW=OFF
  -DOTB_USE_GLFW=OFF
  -DOTB_USE_LIBSVM=ON
  -DOTB_USE_MUPARSER=ON
  -DOTB_USE_MUPARSERX=ON
  -DOTB_USE_OPENMP=ON
  -DOTB_USE_QT=OFF
  -DOTB_USE_QWT=ON
  -DOTB_USE_SHARK=OFF
  -DOTB_USE_OPENCV=ON
  -DOTB_WRAP_PYTHON=ON
  -DOTB_USE_OPENGL=ON
  -DOTB_USE_GLUT=ON
  -DOTB_USE_MPI=ON
  ..;
make -j5 all install;
popd
]]

set_shell_function("build-otb", build_otb_command, build_otb_command)
